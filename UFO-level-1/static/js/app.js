
// from data.js
var tableData = data;

// YOUR CODE HERE!

// Select elements for either submission by click or 'Enter' key press
var button = d3.select("button");
var form = d3.select(".form");

// Run function for either submission by click or 'Enter' key press
button.on("click", runEnter);
form.on("submit",runEnter);

// Create function
function runEnter() {

    // Prevent the page from refreshing
    d3.event.preventDefault();

    // Get input date value
    var inputElement = d3.select(".form-control");
    var inputValue = inputElement.property("value");
    console.log(`Date searched ${inputValue}`);

    // Create filtered data
    var filteredData = data.filter(sightings => sightings.datetime === inputValue);

    // Create individual arrays
    var datetime = filteredData.map(sightings => sightings.datetime);
    var city = filteredData.map(sightings => sightings.city);
    var state = filteredData.map(sightings => sightings.state);
    var country = filteredData.map(sightings => sightings.country);
    var shape = filteredData.map(sightings => sightings.shape);
    var durationMinutes = filteredData.map(sightings => sightings.durationMinutes);
    var comments = filteredData.map(sightings => sightings.comments);

    // Clear the table
    d3.select(".ufo-table")
    .select("tbody").selectAll("tr").remove();
    
    // Add data into table

    for (var i = 0; i < filteredData.length; i++) {
        var tableRow = d3.select(".ufo-table")
        .select("tbody")
        .append("tr");
        tableRow.append("td")
        .text(datetime[i]);
        tableRow.append("td")
        .text(city[i]);
        tableRow.append("td")
        .text(state[i]);
        tableRow.append("td")
        .text(country[i]);
        tableRow.append("td")
        .text(shape[i]);
        tableRow.append("td")
        .text(durationMinutes[i]);
        tableRow.append("td")
        .text(comments[i]);
       };
};

// var button = d3.select("button");

// button.on("click", runEnter);

// function runEnter() {

//     var selectTable = d3.select(".ufo_table");
//     var tbody = selectTable.select("tbody");
//     var newRow = tbody.append("tr");
//     newRow.append("p").text("test");

//     // Prevent the page from refreshing
//     d3.event.preventDefault();
    
//     // Select the input element and get the raw HTML node
//     var inputElement = d3.select("input");
  
//     // Get the value property of the input element
//     var inputValue = inputElement.property("value");
  
//     console.log(inputValue);

//     var filteredTable = tableData.filter(sighting => sighting.datetime === inputValue);

//     console.log(filteredTable);

    
// };

